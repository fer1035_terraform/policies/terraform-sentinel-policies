policy "allow-required-tags" {
    source            = "./allow-required-tags.sentinel"
    enforcement_level = "hard-mandatory"
}

import "module" "tfplan-functions" {
    source = "../../functions/tfplan-functions.sentinel"
}
