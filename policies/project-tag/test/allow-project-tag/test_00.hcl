mock "tfplan/v2" {
  module {
    source = "../../../../mocks/mock-tfplan-v2.sentinel"
  }
}

import "module" "tfplan-functions" {
    source = "../../../../functions/tfplan-functions.sentinel"
}

import "module" "project-tag-map" {
    source = "../../project-tag-map.sentinel"
}

test {
    rules = {
        main = true
    }
}
