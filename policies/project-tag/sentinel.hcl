policy "allow-project-tag" {
    source            = "./allow-project-tag.sentinel"
    enforcement_level = "hard-mandatory"
}

import "module" "tfplan-functions" {
    source = "../../functions/tfplan-functions.sentinel"
}

import "module" "project-tag-map" {
    source = "./project-tag-map.sentinel"
}
