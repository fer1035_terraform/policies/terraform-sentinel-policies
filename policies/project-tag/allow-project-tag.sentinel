import "tfplan/v2" as tfplan
import "tfplan-functions" as plan
import "project-tag-map" as project_tag_map

# Check Caller ID (so you won't get scammed).
allow_caller_arn = rule {
	keys(tfplan.planned_values.outputs) contains "caller_arn"
}

# Get Caller ARN to check for project key ownership.
callerARN = tfplan.planned_values.outputs.caller_arn.value

# Get project value configuration.
projectKeyValue = tfplan.raw.configuration.provider_config.aws.expressions.default_tags[0].tags.constant_value.project

# Check project tag ownership.
allow_caller_project = rule {
	project_tag_map.projectTagValues[callerARN] contains projectKeyValue
}

# Get resource_changes tags_all before and after project values for taggable resources.
allResources = filter tfplan.resource_changes as _, resource_changes {
	resource_changes.mode is "managed" and
	resource_changes.change.after is not null and
	keys(resource_changes.change.after) contains "tags" and
	(resource_changes.change.actions is ["create"] or
		resource_changes.change.actions is ["update"] or
		resource_changes.change.actions is ["no-op"])
}

# Check tags for project key.
allow_project_key = rule {
	all allResources as _, resources {
		keys(resources.change.after) contains "tags_all" and
		keys(resources.change.after.tags_all) contains "project" and
		(resources.change.after.tags is null or
			keys(resources.change.after.tags) not contains "project")
	}
}

# Check project tag values against default_tags.
allow_matching_default = rule {
	all allResources as _, resources {
		resources.change.after.tags_all.project == projectKeyValue
	}
}

# Check project tag values before and after.
allow_matching_before_after = rule {
	all allResources as _, resources {
		resources.change.before is null or
		resources.change.after.tags_all.project == resources.change.before.tags_all.project
	}
}

# Allow matching project tags and previously untagged resources.
main = rule {
	allow_caller_arn and
	allow_caller_project and
	allow_project_key and
	allow_matching_default and
	allow_matching_before_after
}
