# terraform-sentinel-policies

More information about using Sentinel policies can be found at the following links:

- https://www.terraform.io/cloud-docs/sentinel/manage-policies

- https://www.terraform.io/cloud-docs/sentinel/examples

## Build and Test

- Install Sentinel locally.

- To run local tests for the policies, run the following (you'll need your own mock data in place):

    ```bash
    cd policies/required-tags
    sentinel test -verbose allow-required-tags.sentinel
    ```
